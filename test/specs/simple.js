// example of a test group
// note: all tests under the test directory are ran

const topAccos = `http://localhost:3000/oostenrijk/tirol/achenkirch/alpencaravanpark-achensee/camping
http://localhost:3000/oostenrijk/karinthie/dobriach-am-millstatter-see/burgstaller/camping
http://localhost:3000/oostenrijk/karinthie/dobriach-am-millstatter-see/schwimmbadcamping-mossler/camping
http://localhost:3000/oostenrijk/tirol/natters/natterer-see/camping
http://localhost:3000/oostenrijk/salzburg/st-martin-bei-lofer/grubhof-lofer/camping
http://localhost:3000/oostenrijk/karinthie/dellach-im-drautal/am-waldbad/camping
http://localhost:3000/oostenrijk/karinthie/dobriach-am-millstatter-see/brunner-am-see/camping
http://localhost:3000/oostenrijk/karinthie/hermagor/naturpark-schluga-seecamping/camping
http://localhost:3000/oostenrijk/tirol/fugen-im-zillertal/hell/camping
http://localhost:3000/oostenrijk/karinthie/dobriach-am-millstatter-see/seecamping-mossler/camping
http://localhost:3000/oostenrijk/karinthie/millstatt/neubauer/camping
http://localhost:3000/oostenrijk/karinthie/reisach/alpenferienpark-reisach/camping
http://localhost:3000/oostenrijk/stiermarken/sankt-peter-am-kammersberg/bella-austria/camping
http://localhost:3000/oostenrijk/salzburg/bruck-an-der-groglocknerstrae/sportcamp-woferlgut/camping
http://localhost:3000/oostenrijk/karinthie/ossiach/parth-welness-see-camping/camping
http://localhost:3000/oostenrijk/karinthie/kotschach/alpencamp/camping
http://localhost:3000/oostenrijk/karinthie/malta/maltatal/camping
http://localhost:3000/oostenrijk/karinthie/ossiach/terrassencamping-ossiacher-see/camping
http://localhost:3000/oostenrijk/karinthie/millstatt/gauglerhof/camping
http://localhost:3000/oostenrijk/karinthie/bad-kleinkirchheim/landal-bad-kleinkirchheim/vakantiepark
http://localhost:3000/oostenrijk/karinthie/landskron/seecamping-berghof/camping
http://localhost:3000/oostenrijk/salzburg/rauris-taxenbach-embach/oberhasenberghof/camping
http://localhost:3000/oostenrijk/karinthie/st-margareten/rosental-roz/camping
http://localhost:3000/oostenrijk/vorarlberg/nenzing/alpencamping-nenzing/camping
http://localhost:3000/oostenrijk/karinthie/hermagor/schluga-hermagor/camping
http://localhost:3000/oostenrijk/salzburg/bad-gastein/kur-camping-appartementen-erlengrund/camping
http://localhost:3000/oostenrijk/karinthie/oberdrauburg/oberdrauburg/camping
http://localhost:3000/oostenrijk/karinthie/ossiach/ideal-lampele/camping
http://localhost:3000/oostenrijk/tirol/kramsach/seeblick-toni/camping
http://localhost:3000/oostenrijk/karinthie/feistritz-im-rosental/juritz/camping`.split('\n');

describe('Acco page', () => {

  it('Go through tabs', () => {
    const waifForPageLoaded = () => {
      return browser.executeAsync(function (done) {
        zooverPageReady().then(() => {
          done(true)
        })
      });
    };

    topAccos.forEach((item) => {
      browser.url(item)
      browser.pause(1500);

      browser.click('[data-qa="e-nav-fotos"]');
      browser.pause(1500);

      browser.click('[data-qa="e-nav-tips"]');
      browser.pause(1500);

      browser.click('[data-qa="e-nav-forum"]');
      browser.pause(1500);

      browser.click('[data-qa="e-nav-weer"]');
      browser.pause(500);

      browser.click('[data-qa="e-nav-informatie"]');
      browser.pause(500);

      browser.click('[data-qa="e-nav-plattegrond"]');
      browser.pause(500);
    });

    expect(true).to.be.true;
  });
});
